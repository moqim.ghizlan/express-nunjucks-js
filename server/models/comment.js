const mongoose = require('mongoose');
const Schema = mongoose.Schema

const Comment = new Schema({
    content: String,
    userId: String,
    rating: Number,
    cardId: String
})

exports.default = mongoose.model('Comment', Comment)