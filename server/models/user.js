const mongoose = require('mongoose');
const Schema = mongoose.Schema

const User = new Schema({
    username: String,
    email: String,
    password: String
})

exports.default = mongoose.model('User', User)