const mongoose = require('mongoose');
const Schema = mongoose.Schema

const Card = new Schema({
    name: String,
    price: Number,
    imagePath: String,
    brandId: String,
    rating: Number
})

exports.default = mongoose.model('Card', Card)