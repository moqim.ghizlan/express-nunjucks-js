const mongoose = require('mongoose');
const Schema = mongoose.Schema

const Brand = new Schema({
    name: String
})

exports.default = mongoose.model('Brand', Brand)