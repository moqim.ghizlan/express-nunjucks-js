const needConnection = require('../../middlewares/needConnection')['default']
const Brand = require('../models/brand')['default'];

module.exports = (app) => {
    app.post('/api/brand/remove', needConnection, async (req, res) => {
        const { brandId } = req.body
        await Brand.findOneAndDelete({ _id: brandId })

        req.session.message = `Marque supprimée !`
        req.session.save()

        res.writeHead(302, {
            'Location': '/remove-brand'
        }).end()
    })


    app.post('/api/brand/add', needConnection, async (req, res) => {
        const { name } = req.body

        const inst = new Brand()

        inst.name = name

        inst.save((err) => {
            console.log('err', err)
        })

        req.session.message = `Marque ajoutée !`
        req.session.save()

        res.writeHead(302, {
            'Location': '/'
        }).end()
    })

}