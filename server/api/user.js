const needConnection = require('../../middlewares/needConnection')['default']
const User = require('../models/user')['default'];
const bcrypt = require("bcrypt");

module.exports = (app) => {
    app.get('/api/user/remove', needConnection, async (req, res) => {
        const userId = req.session.user._id

        req.session.user = {}
        req.session.save()

        await User.deleteOne({ _id: userId })

        res.writeHead(302, {
            'Location': '/'
        }).end()
    })

    app.get('/api/user/logout', needConnection, async (req, res) => {
        console.log('logout')
        req.session.user = {}
        req.session.save()

        res.writeHead(302, {
            'Location': '/'
        }).end()
    })



    app.post('/api/user/login', async (req, res) => {
        const { login, password } = req.body

        req.session.user = {}
        req.session.save()

        const user = await User.findOne({ $or: [{ 'username': login }, { 'email': login }] })

        if (!user) {
            res.writeHead(302, {
                'Location': '/login',
            }).end()
            req.session.error = 'Utilisateur introuvable !'
            req.session.save()
            return
        }
        const compare = await bcrypt.compare(password, user.password)
        if (!compare) {
            res.writeHead(302, {
                'Location': '/login',
            }).end()
            req.session.error = 'Mauvais mot de passe !'
            req.session.save()
            return
        }
        // return res.status(400).send({ error: 'Bad password' })

        res.writeHead(302, {
            'Location': '/',
        }).end()


        req.session.user = user
        req.session.save()
    })


    const validateEmail = (email) => {
        return String(email)
            .toLowerCase()
            .match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
    }

    app.post('/api/user/register', async (req, res) => {
        req.session.user = {}
        req.session.save()

        let { username, email, password, confirmPassword } = req.body



        if (username.length < 4 || username.length > 32 || !validateEmail(email) || password.length < 8
            || password.length > 32 || confirmPassword !== password) {
            res.writeHead(302, {
                'Location': '/register',
            }).end()
            req.session.error = 'Erreur de validation des champs !'
            req.session.save()
            return
        }

        const found = await User.findOne({ $or: [{ 'username': username }, { 'email': email }] })
        if (found) {
            res.writeHead(302, {
                'Location': '/register',
            }).end()
            req.session.error = 'Le nom d\'utilisateur ou l\'adresse email existe déjà !'
            req.session.save()
            return
        }
        password = await bcrypt.hash(password, 10)

        const inst = new User()

        inst.username = username
        inst.email = email
        inst.password = password

        inst.save((err) => {
            console.log('err', err)
        })

        const user = await User.findOne({ username })

        res.writeHead(302, {
            'Location': '/'
        }).end()

        req.session.user = user
        req.session.save()
    })
}