const needConnection = require('../../middlewares/needConnection')['default']
const Comment = require('../models/comment')['default'];
const Card = require('../models/card')['default'];

module.exports = (app) => {
    app.post('/api/comment/add', needConnection, async (req, res) => {
        const { content, cardId, rating } = req.body

        const inst = new Comment()

        inst.content = content
        inst.cardId = cardId
        inst.rating = rating
        inst.userId = req.session.user._id

        inst.save((err) => {
            console.log('err', err)
        })

        req.session.message = `Commentaire ajouté !`
        req.session.save()

        res.writeHead(302, {
            'Location': req.headers.referer
        }).end()


        let ratingCard = 0;
        let comments = await Comment.find({ cardId: cardId })

        for (let comment of comments)
            ratingCard += comment.rating

        if (comments.length == 0) comments.length = 1
        ratingCard /= comments.length
        ratingCard = Math.round(ratingCard)

        await Card.findOneAndUpdate({ _id: cardId }, {
            rating: ratingCard
        })
    })
}