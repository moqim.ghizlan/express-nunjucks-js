const needConnection = require('../../middlewares/needConnection')['default']
const Comment = require('../models/comment')['default'];
const Card = require('../models/card')['default'];

const multer = require('multer')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images/cards/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({ storage: storage })
const https = require('https')


module.exports = (app) => {
    app.post('/api/card/add', needConnection, upload.single('image'), async (req, res) => {
        let { name, price, brandId, brand, cardImageInput } = req.body

        if (brandId == 'new-brand') {
            const found = await Brand.findOne({ name: brand })
            if (!found) {
                const newBrand = new Brand()
                newBrand.name = brand
                await newBrand.save()
                brandId = newBrand._id
            } else brandId = brand._id
        }

        const inst = new Card()


        if (!req.file) req.file = { path: 'public/image-placeholder.jpg' }

        if (cardImageInput) {
            req.file = { path: `public/images/cards/${Date.now()}.jpg` }
            const file = fs.createWriteStream(req.file.path)
            https.get(cardImageInput, (response) => {
                response.pipe(file)
                file.on('finish', () => {
                    file.close()
                })
            })
        }


        const imagePath = req.file.path

        inst.name = name
        inst.price = price
        inst.imagePath = imagePath
        inst.brandId = brandId

        inst.save()

        req.session.message = `Carte ajoutée !`
        req.session.save()

        res.writeHead(302, {
            'Location': '/'
        }).end()
    })





    app.post('/api/card/save', needConnection, upload.single('image'), async (req, res) => {
        let { name, price, brandId, brand, cardId } = req.body

        if (brandId == 'new-brand') {
            const found = await Brand.findOne({ name: brand })
            if (!found) {
                const newBrand = new Brand()
                newBrand.name = brand
                await newBrand.save()
                brandId = newBrand._id
            } else brandId = brand._id
        }

        if (!req.file) req.file = { path: (await Card.findOne({ _id: cardId })).imagePath }

        const imagePath = req.file.path

        await Card.findOneAndUpdate({ _id: cardId }, {
            name,
            price,
            imagePath,
            brandId
        })

        req.session.message = `Carte modifiée !`
        req.session.save()

        res.writeHead(302, {
            'Location': req.headers.referer
        }).end()
    })





    app.post('/api/card/get', async (req, res) => {
        console.log('get')
        const { brandId, orderBy } = req.body

        let cards = await Card.aggregate([
            {
                $addFields: {
                    "brand": { "$toString": "" },
                    // "addedDate": { "$toString": "" },
                }
            },
        ])

        if (brandId) cards = cards.filter(card => card.brandId == brandId)
        cards = cards.reverse()

        for (let card of cards) {
            card.brand = brandsGl[card.brandId]
            // card.addedDate = card._id.getTimestamp()
        }

        cards = cards.sort((a, b) => {
            if (orderBy == 'name')
                if (a.name < b.name) return -1
                else return 1

            if (orderBy == 'brand')
                if (a.brand < b.brand) return -1
                else return 1

            if (orderBy == 'price')
                if (a.price < b.price) return -1
                else return 1

            // if (orderBy == 'added-date')
            //   if (a.addedDate < b.addedDate) return 1
            //   else return -1

            if (orderBy == 'ratings')
                if (a.ratings < b.ratings) return -1
                else return 1
        })

        console.log('cards', cards)

        res.status(200).send(cards)
    })


    const getMetaData = require('metadata-scraper')


    app.post('/api/card/url', async (req, res) => {
        const { cardUrl } = req.body
        let metadata = await getMetaData(cardUrl)

        res.status(200).send(metadata)
    })







    app.post('/api/card/remove', needConnection, async (req, res) => {
        const { cardId } = req.body

        await Card.findOneAndDelete({ _id: cardId }).then(card => {
            const imagePath = card.imagePath
            if (!imagePath.includes('image-placeholder'))
                fs.unlink(imagePath, err => {
                    if (err) console.log('err', err)
                })
        })

        await Comment.deleteMany({ cardId })

        req.session.message = `Carte supprimée !`
        req.session.save()

        res.writeHead(302, {
            'Location': '/'
        }).end()
    })

}