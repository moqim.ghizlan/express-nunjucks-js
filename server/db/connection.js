const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config({ path: 'config.env' });
mongoose.set("strictQuery", false);

exports.default = async () => {
    try {
        const conn = await mongoose.connect(process.env.MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            // useCreateIndex: true,
            // useFindAndModify: false,
        })


        // conn.comments.drop()


        // mongoose.connection.db.listCollections().toArray(function (err, names) {
        // console.log(names)
        // });


        console.log(`MongoDB connected on: http://${conn.connection.host}:${conn.connection.port}`);
    } catch (error) {
        console.log(error);
        process.exit(1);
    }
}