# Cartes Graphiques

## Infos

- Tailwind a été utilisé pour le CSS du frontend.
- NodeJS et MongoDB ont utilisés pour le backend.
- On peut ajouter des cartes graphiques, les voir, les modifier et les supprimer.
- Les cartes graphiques ont un nom, un prix, un chemin d'image, un ID pour la marque et un nombre moyen d'étoiles (via les commentaires).
- On peut créer des commentaires sur les cartes graphiques avec une note allant de 1 à 5.
- Les cartes graphiques sont rattachés à des marques (créables / supprimables).
- Un filtre par marque et un tri par nom, fabricant, prix, date d'ajout et avis est disponible.
- On peut préremplir les champs "titre" et "image" lors de la création d'une nouvelle carte graphique via une URL (metadonnées).
- Une vérification est effectuée pour la validation des champs pour la connexion / l'inscription.
- La structure des fichiers du projet est optimisée et organisée (les fichiers API / middlewares / models sont dans des répertoires distincts)

## Installation

```shell
yarn install
```

## Lancement

Pour lancer le serveur :
```shell
node server.js
```

Facultatif : Pour lancer Tailwind CSS en mode watch :
```shell
npx tailwindcss -i ./public/input.css -o ./public/output.css --watch
```

### Front

- Tainwind
- EJS

### Back

- NodeJS
- Express
- MongoDB
