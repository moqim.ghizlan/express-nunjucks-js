exports.default = (req, res, next) => {
    if (!req.session.user || !Object.keys(req.session.user).length) {
        req.session.message = 'Vous devez être connecté pour effectuer cette action !'
        req.session.save()
        res.writeHead(302, {
            'Location': req.headers.referer
        }).end()
        return
    }
    next()
}