const path = require("path"); // gestion fichiers locaux
const express = require("express"); //framework mvc
const session = require("express-session"); // sessions
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
require("./server/db/connection")['default']();

const config = require(path.join(__dirname, "config.js"));
dotenv.config({ path: "config.env" });


const User = require('./server/models/user')['default'];
const Card = require('./server/models/card')['default'];
const Brand = require('./server/models/brand')['default'];
const Comment = require('./server/models/comment')['default'];


const app = express();

app.use(
  session({
    secret: process.env.SESSION_SECRET_KEY,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
  })
);
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "/views")));
app.use("/public", express.static("public"));

require('./server/api/user')(app)
require('./server/api/comment')(app)
require('./server/api/card')(app)
require('./server/api/brand')(app)


const fs = require('fs');
const { ObjectId } = require("mongodb");

ObjectId.prototype.getTimestamp = function () {
  return new Date(parseInt(this.toString().slice(0, 8), 16) * 1000);
}

var brandsGl = {}

app.get('*', async (req, res) => {
  const brands = await Brand.find()

  for (let brand of brands) brandsGl[brand._id] = brand.name

  let cards = await Card.aggregate([
    {
      $addFields: {
        "brand": { "$toString": "" },
      }
    },
  ])

  cards = cards.reverse()

  let card = {}
  for (let card of cards) card.brand = brandsGl[card.brandId]

  let page = req._parsedOriginalUrl.pathname
  if (page == '/')
    page = '/index'

  page = page.split('/')[1]

  if (fs.existsSync(`views/${page}.ejs`) && !req._parsedOriginalUrl.pathname.includes('public')) {
    if (page == 'card') {
      const cardId = req._parsedOriginalUrl.pathname.split('/')[2]

      card = await Card.aggregate([
        {
          $addFields: {
            "cardId": { "$toString": "$_id" },
            "brand": { "$toString": "" },
          }
        },
        { $match: { cardId } },

        {
          $lookup:
          {
            from: 'comments',
            localField: 'cardId',
            foreignField: 'cardId',
            as: 'comments',
          },
        },
      ])

      card = card[0]
      let usernames = {}

      if (card) {
        for (let comment of card.comments) {
          comment.createdAt = comment._id.getTimestamp();
          if (!usernames[comment.userId])
            usernames[comment.userId] = (await User.findOne({ _id: new ObjectId(comment.userId) }))['username']
          comment.username = usernames[comment.userId]
        }

        card.brand = brandsGl[card.brandId]
      }
    }
  } else page = '404'

  let comments = await Comment.aggregate([
    {
      $addFields: {
        "username": { "$toString": "" },
        "addedDate": { "$toString": "" },
        "cardName": { "$toString": "" },
      }
    },
  ]).limit(20)


  for (let comment of comments) {
    comment.username = (await User.findOne({ _id: new ObjectId(comment.userId) }))['username']
    comment.createdAt = comment._id.getTimestamp()
    comment.cardName = (await Card.findOne({ _id: new ObjectId(comment.cardId) }))['name']
  }

  let lastComments = comments.slice(0, 4)

  res.render(page, {
    user: req.session.user,
    error: req.session.error,

    cards,

    lastAdded: cards.slice(0, 4),
    ourSelection: cards.sort((a, b) => a.ratings > b.ratings ? 1 : -1).slice(0, 4),

    comments,
    lastComments,

    card,
    message: req.session.message,
    brands,

    isConnected: (!req.session.user || !Object.keys(req.session.user).length ? false : true)
  })

  req.session.error = ''
  req.session.message = ''
  req.session.save()
})



app.listen(process.env.SERVER_PORT, process.env.SERVER_IP, () => {
  console.log(
    `Server running on: http://${process.env.SERVER_IP}:${process.env.SERVER_PORT}`
  );
});